window.CONFIG = {
  // dataFileName: 'demo-submission.html',
  fields: [
    { name: 'id', label: 'id' },
    { name: '家長與校方是否支持您投入資訊領域、您自己是否喜歡資訊領域？', label: '家長與校方是否支持、您自己是否喜歡資訊領域？' },
    { name: '目前一週大約花多少時間在：(1) 學校課業 (不含上學時間)(2) 校外學科補習(3) 除此之外的其他活動(才藝、社團等經常性的行程)。', label: '目前一週：(1) 學校課業 (2) 校外學科補習(3) 其他活動' },
    { name: '您覺得參加資訊之芽語法班會學到什麼呢？為什麼想要學習程式設計？請簡述報名資訊之芽的動機。', label: '報名資芽的動機。' },
    { name: '請問您針對目前資訊之芽的宣傳管道，有沒有任何建議或其他推薦的宣傳方式呢？', label: '推薦的宣傳方式' },
    { name: '程式設計與算數學不同，需要花費大量的時間構思以外，還可能耗掉大量時間除去撰寫的錯誤，甚至可能每週花 3~5 小時或更多（不含上課時間）(視班別與階段而定)。您願意花費這麼多時間嗎？為什麼？', label: '您願意多花時間嗎？為什麼？' },
    { name: '由於 2021 年資訊之芽應會收費，為了避免同學因為經濟負擔而無法參與這個課程，若資源允許，我們會嘗試給予這些同學獎助學金。如果有獎助學金，您是否會有這方面的需求呢？如果是，請在此欄詳述您的狀況，我們會藉由電訪了解您的情況，決定是否給予個別的學費減免；如果否，此欄請留白。', label: '學費減免' },
    { name: '到目前（看到本題）為止，您估計您最多願意在此一活動（資訊之芽）中花費多少的時間呢？(每週可花多少時間(HR)？)', label: '每週花多少時間' },
    { name: '在參加資訊之芽語法班前，您有沒有在其他地方接觸過程式設計？如果有，談談您接觸程式設計的經驗吧！', label: '談談程式設計的經驗' },
    { name: '請問您如何得知資訊之芽？', label: '如何得知資訊之芽？' },
    { name: '"請問您會參與下列哪一場的語法班考試呢？（參加哪一場考試跟報名哪一區的語法班無關）', label: '請問您會參與下列哪一場的語法班考試呢？' },
  ],
  skipRows: 2
}

// vim: et sw=2
