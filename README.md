# Google Sheets Submission Review Tool
Fork From : https://github.com/sitcon-tw/submission-review
## Why

Our conference using Google form as submission system. Reviewing submissions on Google sheets is
not comfortable at all. So I wrote this simple app to display submissions on the web.

## Screenshot

![Screenshot](![](https://i.imgur.com/qunNSVs.png)

## How To Use

Here's a
[sample gsheet](https://docs.google.com/spreadsheets/d/13YHkHXf2MN0dhTRIV3mbOyWSLQYtwVBaMDEd-9g11v8/edit?usp=sharing),
please download this (your) sheets as zipped-HTML format, then open index.html and drag the HTML
format sheet into your browser.

## License

[MIT License](LICENSE)
